# Java

This repository contains all knowledge and my take notes while wrapping my head around new concepts of Java.


## Sources

1. Java SE 8 for really impatient - _It just gives me a catch-up about some new features introduced in Java 8. Not commend if you would like to dig deeper_
2. [Short guideline](https://winterbe.com/posts/2014/03/16/java-8-tutorial/) - I found this resource while looking for a term in Google, it lists out the summary points of Java 8.