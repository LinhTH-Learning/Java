# Definition `Lambdas expresion`
A *lambdas expression* can be understood as a anonymous function:
* `Anonymous` - it doesn't have an explicit name.
* `Function` - it isn't associated with a particular class like a method.
* `Passed around` - can be passed as **an argument** to a method or stored in a variable.
* `Concise` - do not need to write a lot of **boilerplate** like you do for anonymous classes.

It has three ingredients:
1. A block of code
2. Parameters
3. Values for *free* variables. they are not parameters and not defined inside the code.

## Note

* It is illegal for a lambda expression to return a value in some branches but not in others.
For example, `(int x) -> if (x >= 0) return 1;`
* It is the best to think of a lambda expression as a `function`, not an object. It can be passed to a `Functional Interface`
* Never specify the result type of a lambda expression

## QUIZ

### Lambda Syntax
**Which ones are not valid lambda expressions?**
1. () -> {} ✔
2. () -> "Linh Truong" ✔
3. () -> { return "Mario"; } ✔
4. (Integer i) -> return "Alan" + i; 👎
    // `return` *is a control-flow statement. Fix: {return "Alan" + i;}*
5. (String s) -> { "Iron Man"; } 👎
    *"Iron Man" is an expression not statement.*
    
---

# Variable Scope

## Example 1

```java
public static void repeatMessage(String text, int count) {
    Runnable r = () -> {
        count = text.length // Error:...
        for (int i; i <  count; i++) {
            // 
        }   
    }
    new Thread(r).start();
}

repeatMessage("Hello", 1000);
```
`text` and `count` are NOT defined inside the lambda expression but being passed as parameters. -> They are *free* variables.

💡 the Data structure representing the lambda expression must store the values for these variables (e.g "text" and 1000). We could say these values have been _captured_ by the lambda expression. 
Lamdba expression is translated to an object with a single method and the values of the free variables are **copied** into instance variables of that object. Because of that
those variables should be final to avoid inconsistent value.


Therefore, we can not change the value of _free_ variables as not thread safe.

## Example 2

```java
public class StaticReference {
    static int outerStaticNumber = 46;
    int outNumber = 24;

    public void testScope() {
        int localNumber = 1;
        Converter<Integer, String> stringConverter1 = (from) -> {
            outNumber = 23;
            outerStaticNumber = 72;
            localNumber = 2; // ERROR...
            return String.valueOf(from + outNumber);
        };
    }
}
```
* `localNumber` is **copied** when JVM creates a Lambda instance -> should be final. 😶
* `outerStaticNumber` and `outnumber` are fields of the instance of `StaticReference` as the changes to them 
are propagated to the outside class instance. -> it can be changed freely 🏃‍

### Summarize

* We can not change the values of _free_ variables. And should mark Array or Object immutable to avoid any troubles.
* We can change the value of instance fields.
* Default method of a `@FunctionalInterface` can not be accessed from within lambda expression


