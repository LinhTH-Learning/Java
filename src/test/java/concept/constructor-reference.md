# Constructor Reference

Constructor Reference is just like method reference, except that the name of the method is `new`

```java
    List<String> names = ...;
    Stream<Student> stream = names.stream().map(Student::new);
```

## What it to rescue

Overcome a limitation of Java. It is not possible to construct an array of a generic type `T`. The expression `new T[n]` is an error as it would be erased to `new Object[n]`.
For example:

```java
Object[] students = stream.toArray();
```

But we want Student not Object. We can fix by:

```java
Student[] students = stream.toArray(Student[]::new);
```