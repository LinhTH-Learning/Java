# Functional Interface
a *functional interface* is an interface that specifies exactly one abstract method.
e.g.
```java
public interface Comparator<T> {
    int compare(T o1, T o2);
}

public interface Runnable {
    void run();
}

public interface Callable<V> {
    V call() throws Exception;
}
```
> 🎯 if an interface has more than one abstract methods or has no abstract method => it is not an `functional interface`

## **What about `@FunctionalInterface`?**

You can tag any **functional interface** with the `@FunctionalInterface`annotation. This has two advantages:
* The compiler checks that the annotated entity is an interface with a single abstract method.
* And good for Documentation (Javadoc page).

> 👉But it is not mandatory.
