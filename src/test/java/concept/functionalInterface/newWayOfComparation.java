package concept.functionalInterface;

import org.junit.Assert;
import org.junit.Test;

import java.util.Comparator;

public class newWayOfComparation {
    Comparator<Apple> oldByWeight = new Comparator<Apple>() {
        @Override
        public int compare(Apple apple1, Apple apple2) {
            return apple1.getWeight().compareTo(apple2.getWeight());
        }
    };

    /**
     * Can be shortened to Comparator<Apple> newByWeight = Comparator.comparing(Apple::getWeight);
     */
    Comparator<Apple> newByWeight = (apple1, apple2) -> apple1.getWeight().compareTo(apple2.getWeight());

    @Test
    public void test() {
        // given
        Apple apple1 = new Apple(COLOR.GREEN, 150);
        Apple apple2 = new Apple(COLOR.GREEN, 200);

        Assert.assertEquals(oldByWeight.compare(apple1, apple2), newByWeight.compare(apple1, apple2));
    }

    // Given
    public enum COLOR {
        GREEN,
        YELLOW,
        RED
    }

    public class Apple {
        private COLOR color;
        private int weight;

        public Apple(COLOR color, int weight) {
            this.color = color;
            this.weight = weight;
        }

        public COLOR getColor() {
            return color;
        }

        public Integer getWeight() {
            return weight;
        }
    }
}
