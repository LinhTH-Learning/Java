package concept.functionalInterface;


import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

@FunctionalInterface
interface BufferedReaderProcessor {
    String process(BufferedReader b) throws IOException;
}

public class TestProcessingFile {
    private String processFile() throws IOException {
        try(BufferedReader br = new BufferedReader(new FileReader("lambda.md"))) {
            return br.readLine();
        }
    }

    private String processFile(BufferedReaderProcessor p) throws IOException {
        try(BufferedReader br = new BufferedReader(new FileReader("lambda.md"))) {
            return p.process(br);
        }
    }

    @Test
    public void test() throws IOException {
        String oneLineByOldWay = processFile();
        String oneLineByNewWay = processFile(BufferedReader::readLine);

        Assert.assertEquals(oneLineByOldWay, oneLineByNewWay);
    }

    @Test
    public void testMultipleLine() throws IOException {
        String firstLine = processFile(BufferedReader::readLine);
        String twoFirstLine = processFile(br -> br.readLine() + br.readLine());

        Assert.assertNotEquals(firstLine, twoFirstLine);
        Assert.assertTrue(twoFirstLine.contains(firstLine));
    }
}
